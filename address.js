let firstName = "Michael"
let lastName = "Jordan"
let streetAddress = "1234 N 56st St"
let city = "Chicago"
let state = "Illinois"
let zip = 78910

// Address
let addressLine = firstName + " " + lastName + "\n" + streetAddress + "\n" + city + ", " + state + " " + zip

// Extract first name
let firstNameLastIndex = addressLine.indexOf(" ")
let firstNameExtract = addressLine.slice(0,firstNameLastIndex)

// Extract last name
let lastNameLastIndex = addressLine.indexOf("\n")
let lastNameExtract = addressLine.slice(firstNameLastIndex + 1, lastNameLastIndex)

// Extract street address
let streetAddressFirstIndex = addressLine.indexOf("\n") + 1
let streetAddressLastIndex = addressLine.indexOf("\n",streetAddressFirstIndex)
let streetAddressExtract = addressLine.slice(streetAddressFirstIndex,streetAddressLastIndex)

// Extract city
let cityFirstIndex = streetAddressLastIndex + 1
let cityLastIndex = addressLine.indexOf(",")
let cityExtract = addressLine.slice(cityFirstIndex,cityLastIndex)

// Extract state
let stateFirstIndex = cityLastIndex + 2
let stateLastIndex = addressLine.indexOf(" ", stateFirstIndex)
let stateExtract = addressLine.slice(stateFirstIndex,stateLastIndex)

// Extract zip
let zipStartIndex = stateLastIndex + 1;
let zipLastIndex = addressLine.length
let zipExtract = addressLine.slice(zipStartIndex, zipLastIndex)